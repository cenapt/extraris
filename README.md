# Description
Sometimes, when you write article with people, they have no clue on how to share bibliographic citations with you (and often even just paste them from other publications). Dealing with articles containing hundreds of citations is a pain. And sometimes you have to transfer those citations back to other authors that use non-Free systems. 

To deal with that, [Google Scholar](https://scholar.google.com) is currently the best tool to convert text citations to a few candidates (most of the time only 1). I have a shortcut (on Linux/X11) that opens google scholar with the current selection.

Once you get to the publication website, [Zotero](https://www.zotero.org) is the best tool (and it is Free), just click on the paper icon in your webbrowser, and it will be added to your collection.

In my case, I had to produce individual RIS files for each citation according to a code. I decided to add that code in the Extra field of Zotero and make a custom exporter (Bjo-Ris.js) that would put it in the RIS file (making it a BRIS file) the Extra zotero-field as the EX RIS-like-field.

Then a small python script (converter.py) take the BRIS file and produces all the individual RIS files naming them using the EX field.

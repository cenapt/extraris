import argparse
import os
import sys

block = ''


parser = argparse.ArgumentParser(
    description="Convert bjo-ris with the EX field to individual files")

parser.add_argument('input', type=str, nargs=1,
                    help='BRIS file')

parser.add_argument('-o', '--output', type=str, nargs=1,
                    help='Output directory', default=['output'])


args = parser.parse_args()

input_file = args.input[0]
if not os.path.isfile(input_file):
    print('File {} doesn\'t exist'.format(input_file), file=sys.stderr)
    sys.exit(1)


output_dir = args.output[0]
if not os.path.isdir(output_dir):
    print('Making the {} directory'.format(output_dir))
    os.makedirs(output_dir)


with open(input_file, 'r') as f:
    for line in f:
        line_s = line.strip()
        if line_s.startswith('EX'):
            block_name = line_s[6:]
        elif line_s == '':
            with open('{}/{}.ris'.format(output_dir, block_name),
                      'w') as fo:
                fo.write(block)
            block = ''
        else:
            block += line_s + '\r\n'

sys.exit(0)
